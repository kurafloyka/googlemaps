package com.example.mapapps.practise1;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.example.mapapps.R;

public class ChangeFragment {


    Context context;

    public ChangeFragment(Context context) {
        this.context = context;
    }


    public void change(Fragment fragment) {
        ((FragmentActivity) context).getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame, fragment, "fragments")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();

    }
}
