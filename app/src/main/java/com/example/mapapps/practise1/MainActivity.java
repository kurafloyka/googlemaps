package com.example.mapapps.practise1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.mapapps.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ChangeFragment changeFragment = new ChangeFragment(MainActivity.this);
        changeFragment.change(new GoogleMapFragment());
    }
}
